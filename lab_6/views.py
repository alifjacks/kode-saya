from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
response = {}

def index(request) :
    response['author'] = "Alif Fadli"
    html = 'lab_6/lab_6.html'
    return render(request, html, response)
